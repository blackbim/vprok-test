import Vue from 'vue'
import App from './App.vue'
import router from './router'

// Normailize css
import 'normalize.css'

// Main Styles
import '@/assets/less/main.less'

// Vuex Store
import store from './store/store'

Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
